using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glow : MonoBehaviour
{
    [Tooltip("The liquid shader on this GO")]
    [SerializeField] private Material liquidMat;

    [Tooltip("The first color of this liquid")]
    [SerializeField] private Color color1;

    [Tooltip("The second color of this liquid")]
    [SerializeField] private Color color2;

    [Tooltip("The speed of the color change")]
    [SerializeField] private float lerpSpeed = 1.0f;

    private void Start()
    {
        StartCoroutine(changeColor(color1, color2));
    }

    private IEnumerator changeColor(Color prevColor, Color nextColor)
    {
        Color lerpedColor;
        float lerpAmount = 0f;

        while (liquidMat.GetColor("_BaseColor") != nextColor)
        {
            lerpAmount += lerpSpeed * Time.deltaTime;
            lerpedColor = Color.Lerp(prevColor, nextColor, lerpAmount);
            liquidMat.SetColor("_BaseColor", lerpedColor);
            yield return null;
        }
        StartCoroutine(changeColor(nextColor, prevColor));
    }
}
